^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package teraranger
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.1 (2017-09-20)
------------------
* Update package.xml
* Contributors: Kabaradjian PL

1.0.0 (2017-09-15)
------------------
* Create README.md
* Handle infinite values
* Add One and Duo support
* Fix frame dropping on 'T'
* Clean files and change to pragma once
* Implement REP 117 for teraranger duo
* Implement REP 117 for teraranger one
* Use helper lib and clean header of one
* Use helper lib and clean header of duo
* Remove old erial library files
* Update drivers to ros-serial
* Enable outdoor mode in reconfigure for one and duo
* Initialize repository
* Contributors: BaptistePotier, Mateusz Sadowski, pl-kabaradjian
